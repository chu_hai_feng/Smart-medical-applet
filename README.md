# 智慧医疗-在线挂号小程序

#### 介绍
智慧医疗-在线挂号小程序旨在搭建一个医疗在线挂号平台，采用前后端分离的开发模式，本人负责所有的前端部分，该小程序可用于新冠疫苗预约、HPV疫苗预约、图文咨询、预约挂号、健康体检预约、健康自测、医师课堂等应用模块。

主要技术栈: Vue3.2 + vue-cli + TypeScript + uniapp + vite。

#### 小程序演示
![输入图片说明](https://foruda.gitee.com/images/1674114455327552933/9e50bf7b_11241030.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1674114481035887476/401ee694_11241030.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1674114492241621772/ac62f240_11241030.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1674114507031279881/405f9581_11241030.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1674114529700685089/f0673a88_11241030.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1674114540168553478/c5cf3222_11241030.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1674114550768529477/5c7fdda9_11241030.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1674114560628020313/e69e2c96_11241030.png "屏幕截图")


#### 使用教程：

1.打开src/manifest.json文件，微信小程序配置里，更改为你自己的 小程序APPID

2.打开src/pages/login-page/index.vue，第37行， 把appid和secret秘钥改为你自己的小程序appid和秘钥

3.在根目录下执行 npm install 安装依赖，成功后， 再执行npm run dev:mp-weixin

4.打开微信开发者工具，创建项目，目录选择dist/dev/mp-weixin，打开项目后编译即可出现

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


